# SailfishOS for Xiaomi POCO X3 PRO

This repository uses gitlab-ci to build the sailfish images for the Xiaomi Poco X3 PRO


# Files

[vayu.env](https://gitlab.com/sailfishos-porters-ci/vayu-ci/blob/main/vayu.env) contains all the environment variables required for the process. On every release, version number is changed in the master branch and a new tag is created with the release number to trigger the build process.

[Jolla-@RELEASE@-vayu-@ARCH@.ks](https://gitlab.com/sailfishos-porters-ci/vayu-ci/blob/main/Jolla-@RELEASE@-vayu-@ARCH@.ks) file is the kickstart file which is used by PlatformSDK image, this files contains device specific repositories, Repositories can be changed by from devel to testing or vice versa.

[run-mic.sh](https://gitlab.com/sailfishos-porters-ci/vayu-ci/blob/main/run-mic.sh) is a simple bash script, which executes the build.

# Install

1. Install SailfishOS.zip

2. Install [Latest sfos-adaptation-vayu.zip](https://github.com/SailfishOS-vayu/sfos-adaptation-vayu/releases)

# Download

[Download the latest build](https://gitlab.com/sailfishos-porters-ci/vayu-ci/-/jobs?scope=finished)

# Source code
[https://github.com/SailfishOS-vayu](https://github.com/SailfishOS-vayu)
